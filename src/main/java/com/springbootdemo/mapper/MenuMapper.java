package com.springbootdemo.mapper;


import com.springbootdemo.bean.Menu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * created by lipenghui on 2019/4/24
 **/
public interface MenuMapper {
  List<Menu> getAllMenu();
}
