package com.springbootdemo.mapper;

import com.springbootdemo.bean.Position;

import java.util.List;

/**
 * created by lipenghui on 2019/4/29
 **/
public interface PositionMapper {
  List<Position> getAllPos();
}
