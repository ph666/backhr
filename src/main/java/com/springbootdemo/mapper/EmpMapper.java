package com.springbootdemo.mapper;

import com.springbootdemo.bean.Employee;
import com.springbootdemo.bean.Nation;
import com.springbootdemo.bean.PoliticsStatus;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * created by lipenghui on 2019/4/28
 **/
public interface EmpMapper {
  int addEmp(Employee employee);

  int addEmps(@Param("emps") List<Employee> emps);

  List<Nation> getAllNations();

  List<PoliticsStatus> getAllPolitics();

  Long getMaxWorkID();

  List<Employee> getEmployeeByPage(@Param("start") Integer start, @Param("size") Integer size, @Param("keywords") String keywords,
                                   @Param("politicId") Long politicId, @Param("nationId") Long nationId, @Param("posId") Long posId,
                                   @Param("jobLevelId") Long jobLevelId, @Param("engageForm") String engageForm, @Param("departmentId") Long departmentId,
                                   @Param("startBeginDate") Date startBeginDate, @Param("endBeginDate") Date endBeginDate);

  Long getCountByKeywords(@Param("keywords") String keywords,
                          @Param("politicId") Long politicId, @Param("nationId") Long nationId, @Param("posId") Long posId,
                          @Param("jobLevelId") Long jobLevelId, @Param("engageForm") String engageForm, @Param("departmentId") Long departmentId,
                          @Param("startBeginDate") Date startBeginDate, @Param("endBeginDate") Date endBeginDate);
}
