package com.springbootdemo.mapper;

import com.springbootdemo.bean.Hr;
import com.springbootdemo.bean.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * created by lipenghui on 2019/4/23
 **/
public interface HrMapper {
  Hr loadUserByUsername(String username);

  List<Role> getRolesByHrId(Long id);

  int hrReg(@Param("username") String username, @Param("password") String password);
}
