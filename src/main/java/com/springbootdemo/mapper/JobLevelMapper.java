package com.springbootdemo.mapper;

import com.springbootdemo.bean.JobLevel;

import java.util.List;

/**
 * created by lipenghui on 2019/4/29
 **/
public interface JobLevelMapper {
  List<JobLevel> getAllJobLevels();
}
