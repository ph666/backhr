package com.springbootdemo.mapper;

import com.springbootdemo.bean.Department;

import java.util.List;

/**
 * created by lipenghui on 2019/4/29
 **/
public interface DepartmentMapper {
  List<Department> getDepByPid(Long pid);

  List<Department> getAllDeps();
}
