package com.springbootdemo.controller.system;

import com.springbootdemo.service.HrService;
import com.springbootdemo.bean.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * created by lipenghui on 2019/4/24
 **/
@RestController
@RequestMapping("/system/hr")
public class SystemHrController {
  @Autowired
  HrService hrService;

  @RequestMapping(value = "/reg", method = RequestMethod.POST)
  public RespBean hrReg(String username, String password) {
    int i = hrService.hrReg(username, password);
    if (i == 1) {
      return RespBean.success("注册成功");
    } else if (i == -1) {
      return RespBean.error("用户名重复，注册失败");
    }
    return RespBean.error("注册失败");
  }
}
