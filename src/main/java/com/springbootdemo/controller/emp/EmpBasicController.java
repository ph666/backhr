package com.springbootdemo.controller.emp;

import com.springbootdemo.bean.Employee;
import com.springbootdemo.bean.Position;
import com.springbootdemo.bean.RespBean;
import com.springbootdemo.common.EmailRunnable;
import com.springbootdemo.common.poi.PoiUtils;
import com.springbootdemo.service.DepartmentService;
import com.springbootdemo.service.EmpService;
import com.springbootdemo.service.JobLevelService;
import com.springbootdemo.service.PositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.TemplateEngine;
import sun.security.util.PolicyUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

/**
 * created by lipenghui on 2019/4/28
 **/
@RestController
@RequestMapping("/employee/basic")
public class EmpBasicController {
  @Autowired
  EmpService empService;
  @Autowired
  PositionService positionService;
  @Autowired
  DepartmentService departmentService;
  @Autowired
  JobLevelService jobLevelService;
  @Autowired
  ExecutorService executorService;
  @Autowired
  TemplateEngine templateEngine;
  @Autowired
  JavaMailSender javaMailSender;

  @RequestMapping(value = "/emp", method = RequestMethod.POST)
  public RespBean addEmp(Employee employee) {
    if (empService.addEmp(employee) == 1) {
      List<Position> allPos = positionService.getAllPos();
      for(Position allPo : allPos) {
        if (allPo.getId() == employee.getPosId()) {
          employee.setPosName(allPo.getName());
        }
      }
      executorService.execute(new EmailRunnable(employee, javaMailSender, templateEngine));
      return RespBean.success("添加成功！");
    }
    return RespBean.error("添加失败！");
  }

  @RequestMapping(value = "/basicdata", method = RequestMethod.GET)
  public RespBean getBasicData() {
    Map<String, Object> map = new HashMap<>();
    map.put("nations", empService.getAllNations());
    map.put("politics", empService.getAllPolitics());
    map.put("deps", departmentService.getDepByPid(-1L));
    map.put("positions", positionService.getAllPos());
    map.put("joblevels", jobLevelService.getAllJobLevels());
    map.put("workID", String.format("%08d", empService.getMaxWorkID() + 1));
    return RespBean.success("", map);
  }

  @RequestMapping(value = "/emp", method = RequestMethod.GET)
  public RespBean getEmployeeByPage(
          @RequestParam(defaultValue = "1") Integer page,
          @RequestParam(defaultValue = "10") Integer size,
          @RequestParam(defaultValue = "") String keywords,
          Long politicId, Long nationId, Long posId,
          Long jobLevelId, String engageForm,
          Long departmentId, String beginDateScope
  ) {
    Map<String, Object> map = new HashMap<>();
    List<Employee> employeeByPage = empService.getEmployeeByPage(page, size, keywords, politicId, nationId, posId,
            jobLevelId, engageForm, departmentId, beginDateScope);
    Long count = empService.getCountByKeywords(keywords, politicId, nationId, posId, jobLevelId, engageForm, departmentId, beginDateScope);
    map.put("emps", employeeByPage);
    map.put("count", count);
    return RespBean.success("", map);
  }

  @RequestMapping(value = "/exportEmp", method = RequestMethod.GET)
  public ResponseEntity<byte[]> exportEmp() {
    return PoiUtils.exportEmp2Excel(empService.getAllEmployees());
  }

  @RequestMapping(value = "/importEmp", method = RequestMethod.POST)
  public RespBean importEmp(MultipartFile file) {
    List<Employee> emps = PoiUtils.importEmp2List(file,
            empService.getAllNations(), empService.getAllPolitics(),
            departmentService.getAllDeps(), positionService.getAllPos(),
            jobLevelService.getAllJobLevels());
    if (empService.addEmps(emps) == emps.size()) {
      return RespBean.success("导入成功！");
    }
    return RespBean.error("导入失败");
  }
}
