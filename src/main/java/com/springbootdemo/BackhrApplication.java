package com.springbootdemo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@MapperScan("com.springbootdemo.mapper")
@EnableCaching
public class BackhrApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackhrApplication.class, args);
	}

}
