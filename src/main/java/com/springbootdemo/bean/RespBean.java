package com.springbootdemo.bean;

/**
 * created by lipenghui on 2019/4/24
 **/
public class RespBean {
  private String code;
  private String message;
  private Object data;

  private RespBean() {}

  private RespBean(String code, String message, Object data) {
    this.code = code;
    this.message = message;
    this.data = data;
  }

  public static RespBean success(String message, Object data) {
    return new RespBean("0", message, data);
  }

  public static RespBean success(String message) {
    return new RespBean("0", message, null);
  }

  public static  RespBean error(String message, Object data) {
    return new RespBean("1", message, data);
  }

  public static RespBean error(String message) {
    return new RespBean("1", message, null);
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }
}
