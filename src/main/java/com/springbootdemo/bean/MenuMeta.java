package com.springbootdemo.bean;

import java.io.Serializable;

/**
 * created by lipenghui on 2019/4/24
 **/
public class MenuMeta implements Serializable {
  private boolean keepAlive;
  private boolean requireAuth;

  public boolean isKeepAlive() {
    return keepAlive;
  }

  public void setKeepAlive(boolean keepAlive) {
    this.keepAlive = keepAlive;
  }

  public boolean isRequireAuth() {
    return requireAuth;
  }

  public void setRequireAuth(boolean requireAuth) {
    this.requireAuth = requireAuth;
  }
}
