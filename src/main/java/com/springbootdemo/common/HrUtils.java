package com.springbootdemo.common;

import com.springbootdemo.bean.Hr;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * created by lipenghui on 2019/4/24
 **/
public class HrUtils {
  public static Hr getCurrentHr() {
    return (Hr) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
  }
}
