package com.springbootdemo.service;

import com.springbootdemo.bean.JobLevel;
import com.springbootdemo.mapper.JobLevelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * created by lipenghui on 2019/4/29
 **/
@Service
@Transactional
public class JobLevelService {
  @Autowired
  JobLevelMapper jobLevelMapper;

  public List<JobLevel> getAllJobLevels() {
    return jobLevelMapper.getAllJobLevels();
  }
}
