package com.springbootdemo.service;

import com.springbootdemo.bean.Department;
import com.springbootdemo.mapper.DepartmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * created by lipenghui on 2019/4/29
 **/
@Service
@Transactional
public class DepartmentService {
  @Autowired
  DepartmentMapper departmentMapper;

  public List<Department> getDepByPid(Long pid) {
    return departmentMapper.getDepByPid(pid);
  }

  public List<Department> getAllDeps() {
    return departmentMapper.getAllDeps();
  }
}
