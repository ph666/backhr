package com.springbootdemo.service;

import com.springbootdemo.bean.Position;
import com.springbootdemo.mapper.PositionMapper;
import javafx.geometry.Pos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * created by lipenghui on 2019/4/29
 **/
@Service
@Transactional
public class PositionService {
  @Autowired
  PositionMapper positionMapper;

  public List<Position> getAllPos() {
    return positionMapper.getAllPos();
  }
}
