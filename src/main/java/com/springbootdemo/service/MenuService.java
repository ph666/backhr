package com.springbootdemo.service;

import com.springbootdemo.bean.Menu;
import com.springbootdemo.mapper.MenuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * created by lipenghui on 2019/4/24
 **/
@Service
@Transactional
@CacheConfig(cacheNames = "menus_cache")
public class MenuService {
  @Autowired
  MenuMapper menuMapper;

  public List<Menu> getAllMenu() {
    return menuMapper.getAllMenu();
  }

}
