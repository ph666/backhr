CREATE TABLE `role` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(64) DEFAULT NULl,
  `nameZh` VARCHAR(64) DEFAULT NULL COMMENT '角色名称',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8;

CREATE TABLE `hr` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'hrId',
  `name` VARCHAR(32) DEFAULT NULL COMMENT '姓名',
  `phone` char(11) DEFAULT NULL COMMENT '手机号码',
  `telephone` VARCHAR(16) DEFAULT NULL COMMENT '住宅电话',
  `address` VARCHAR(64) DEFAULT NULL COMMENT '联系地址',
  `enabled` TINYINT(1) DEFAULT '1',
  `password` VARCHAR(255) DEFAULT NULL COMMENT '密码',
  `userface` VARCHAR(255) DEFAULT NULL,
  `username` VARCHAR(255) DEFAULT NULL COMMENT '用户名',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET =utf8;

CREATE TABLE `hr_role` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `hrid` INT(11) DEFAULT NULL ,
  `rid` INT(11) DEFAULT NULL ,
  PRIMARY KEY (`id`),
  KEY `rid` (`rid`),
  KEY `hr_role_ibfk_1` (`hrid`),
  CONSTRAINT `hr_role_ibfk_1` FOREIGN KEY (`hrid`) REFERENCES `hr` (`id`) ON DELETE CASCADE ,
  CONSTRAINT `hr_role_ibfk_2` FOREIGN KEY (`rid`) REFERENCES `role` (`id`)
) ENGINE = InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8;

CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` VARCHAR(64) DEFAULT NULL ,
  `path` VARCHAR(64) DEFAULT NULL ,
  `component` VARCHAR(64) DEFAULT NULL ,
  `name` VARCHAR(64) DEFAULT NULL ,
  `iconCls` VARCHAR(64) DEFAULT NULL ,
  `keepAlive` TINYINT(1) DEFAULT NULL ,
  `requireAuth` TINYINT(1) DEFAULT NULL ,
  `parentId` INT(11) DEFAULT NULL ,
  `enabled` TINYINT(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parentId` (`parentId`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parentId`) REFERENCES `menu` (`id`)
) ENGINE = InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `menu_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) DEFAULT NULL ,
  `rid` int(11) DEFAULT NULL ,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`),
  KEY `rid` (`rid`),
  CONSTRAINT `menu_role_ibfk_1` FOREIGN KEY (`mid`) REFERENCES `menu` (`id`),
  CONSTRAINT `menu_role_ibfk_2` FOREIGN KEY (`rid`) REFERENCES `role` (`id`)
) ENGINE = InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8;

CREATE TABLE `employee` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '员工编号',
  `name` VARCHAR(10) DEFAULT NULL COMMENT '员工姓名',
  `gender` char(4) DEFAULT NULL COMMENT '性别',
  `birthday` DATE DEFAULT NULL COMMENT '出生日期',
  `idCard` CHAR(18) DEFAULT NULL COMMENT '出生日期',
  `wedlock` ENUM('已婚','未婚','离异') DEFAULT NULL COMMENT '婚姻状况',
  `nationId` INT(8) DEFAULT NULL COMMENT '民族',
  `nativePlace` VARCHAR(20) DEFAULT NULL COMMENT '籍贯',
  `politicId` INT(8) DEFAULT NULL COMMENT '政府面貌',
  `email` VARCHAR(20) DEFAULT NULL COMMENT '邮箱',
  `phone` VARCHAR(11) DEFAULT NULL COMMENT '电话号码',
  `address` VARCHAR(64) DEFAULT NULL COMMENT '联系地址',
  `departmentId` INT(11) DEFAULT NULL COMMENT '所属部门',
  `jobLevelId` INT(11) DEFAULT NULL COMMENT '职称ID',
  `posId` INT(11) DEFAULT NULL COMMENT '职位ID',
  `engageForm` VARCHAR(8) DEFAULT NULL COMMENT '聘用形式',
  `tiptopDegree` ENUM('博士','硕士','本科','大专','高中','初中','小学','其他') DEFAULT NULL COMMENT '最高学历',
  `specialty` VARCHAR(32) DEFAULT NULL COMMENT '所属专业',
  `school` VARCHAR(32) DEFAULT NULL COMMENT '毕业院校',
  `beginDate` date DEFAULT NULL COMMENT '入职日期',
  `workState` ENUM('在职','离职') DEFAULT '在职' COMMENT '在职状态',
  `workID` CHAR(8) DEFAULT NULL COMMENT '工号',
  `contractTerm` DOUBLE DEFAULT NULL COMMENT '合同期限',
  `conversionTime` DATE DEFAULT NULL COMMENT '转正日期',
  `notWorkDate` DATE DEFAULT NULL COMMENT '离职日期',
  `beginContract` date DEFAULT NULL COMMENT '合同起始日期',
  `endContract` date DEFAULT NULL COMMENT '合同终止日期',
  `workAge` int(11) DEFAULT NULL COMMENT '工龄',
  PRIMARY KEY (`id`),
  KEY `departmentId` (`departmentId`),
  KEY `jobId` (`jobLevelId`),
  KEY `dutyId` (`posId`),
  KEY `nationId` (`nationId`),
  KEY `politicId` (`politicId`),
  KEY `workID` (`workID`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`departmentId`) REFERENCES `department` (`id`),
  CONSTRAINT `employ_ibfk_2` FOREIGN KEY (`jobLevelId`) REFERENCES `joblevel` (`id`),
  CONSTRAINT  `employ_ibfk_3` FOREIGN KEY (`posId`) REFERENCES `position` (`id`),
  CONSTRAINT  `employ_ibfk_4` FOREIGN KEY (`nationId`) REFERENCES `nation` (`id`),
  CONSTRAINT `employ_ibfk_5` FOREIGN KEY (`politicId`) REFERENCES `politicsstatus` (`id`)
) ENGINE = InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `department` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(32) DEFAULT NULL COMMENT '部门名称',
  `parentId` int(11) DEFAULT NULL ,
  `depPath` VARCHAR(255) DEFAULT NULL ,
  `enabled` TINYINT(1) DEFAULT '1',
  `isParent` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `joblevel` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(32) DEFAULT NULL COMMENT '职称名称',
  `titleLevel` ENUM('正高级','副高级','中级','初级','员级') DEFAULT NULL ,
  `createDate` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `enabled` TINYINT(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8;

CREATE TABLE `position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(32) DEFAULT NULL COMMENT '职位',
  `createDate` TIMESTAMP NULL DEFAULT current_timestamp,
  `enabled` TINYINT(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE = InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8;

CREATE TABLE `nation` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(32) DEFAULT NULL ,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8;

CREATE TABLE `politicsstatus` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(32) DEFAULT NULL ,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8;

